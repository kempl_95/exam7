package kg.exam.foodservice.dto;

import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
//Мы можем из любого заведения заказать любое из блюд - Условие такое, так можно было бы обойтись одним id блюда. По имени нет смысла так как могут быть совпадения. GenerateData.Random может дать 2 одинаковых имени в одном заведении
public class AddOrderDTO {
    private String placeId;
    private String dishId;
}
