package kg.exam.foodservice.dto;

import kg.exam.foodservice.model.Dish;
import kg.exam.foodservice.model.Order;
import kg.exam.foodservice.model.User;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class OrderDTO {

    public static OrderDTO from(Order order) {
        return builder()
                .id(order.getId())
                .user(order.getUser())
                .dish(order.getDish())
                .date(order.getDate())
                .build();
    }

    private String id;
    private User user;
    private Dish dish;
    private String date;

}
