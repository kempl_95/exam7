package kg.exam.foodservice.dto;

import kg.exam.foodservice.model.Dish;
import kg.exam.foodservice.model.Place;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class DishDTO {

    public static DishDTO from(Dish dish) {
        return builder()
                .id(dish.getId())
                .name(dish.getName())
                .type(dish.getType())
                .price(dish.getPrice())
                .place(dish.getPlace())
                .build();
    }

    private String id;
    private String name;
    private String type;
    private int price;
    private Place place;

}
