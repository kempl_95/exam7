package kg.exam.foodservice.service;

import kg.exam.foodservice.dto.UserDTO;
import kg.exam.foodservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    UserRepository users;

    public Slice<UserDTO> findUsers(Pageable pageable) {
        var slice = users.findAll(pageable);
        return slice.map(UserDTO::from);
    }

}
