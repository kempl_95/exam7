package kg.exam.foodservice.service;

import kg.exam.foodservice.dto.*;
import kg.exam.foodservice.model.Order;
import kg.exam.foodservice.model.User;
import kg.exam.foodservice.repository.DishRepository;
import kg.exam.foodservice.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orders;
    @Autowired
    private DishRepository dishes;

    public Slice<OrderDTO> findOrders(Pageable pageable) {
        var slice = orders.findAll(pageable);
        return slice.map(OrderDTO::from);
    }

    public ResponceDTO addOrder(Authentication authentication, AddOrderDTO data){
        try{
            var dishList = dishes.getFirstByIdAndPlaceId(data.getDishId() ,data.getPlaceId());
            User user = (User)authentication.getPrincipal();
            var order = Order.builder()
                    .user(user)
                    .dish(dishList.get(0))
                    .date(LocalDateTime.now().toString())
                    .build();
            orders.save(order);
            return ResponceDTO.from(order.getId(), "Order " + order.getId() + " created Successfully");
        } catch (java.lang.IndexOutOfBoundsException ex){
            return ResponceDTO.from("400", "Error! Didn't found this dish");
        }
    }
    //Мы можем просмотреть список своих заказов
    public Slice<OrderDTO> findAuthUserOrders(Authentication authentication, Pageable pageable) {
        var user = (User)authentication.getPrincipal();
        var slice = orders.findAllByUserId(user.getId(), pageable);
        return slice.map(OrderDTO::from);
    }
}
