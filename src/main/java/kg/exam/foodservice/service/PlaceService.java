package kg.exam.foodservice.service;

import kg.exam.foodservice.dto.DishDTO;
import kg.exam.foodservice.dto.PlaceDTO;
import kg.exam.foodservice.dto.UserDTO;
import kg.exam.foodservice.repository.DishRepository;
import kg.exam.foodservice.repository.PlaceRepository;
import kg.exam.foodservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class PlaceService {
    @Autowired
    PlaceRepository places;
    @Autowired
    DishRepository dishes;
    //Мы можем получать список заведений.
    public Slice<PlaceDTO> findPlaces(Pageable pageable) {
        var slice = places.findAll(pageable);
        return slice.map(PlaceDTO::from);
    }
    //По каждому заведению мы можем просматривать список блюд.
    public Slice<DishDTO> findPlaceDishes(String id, Pageable pageable) {
        var slice = dishes.findAllByPlaceId(id, pageable);
        return slice.map(DishDTO::from);
    }
}
