package kg.exam.foodservice.service;

import kg.exam.foodservice.dto.DishDTO;
import kg.exam.foodservice.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class DishService {
    @Autowired
    DishRepository dishes;

    public Slice<DishDTO> findDishes(Pageable pageable) {
        var slice = dishes.findAll(pageable);
        return slice.map(DishDTO::from);
    }
}
