package kg.exam.foodservice.repository;

import kg.exam.foodservice.model.Dish;
import kg.exam.foodservice.model.Place;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PlaceRepository extends PagingAndSortingRepository<Place, String> {
}
