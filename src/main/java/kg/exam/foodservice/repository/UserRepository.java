package kg.exam.foodservice.repository;

import kg.exam.foodservice.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User, String> {
    public Optional<User> getByEmail(String s);
}
