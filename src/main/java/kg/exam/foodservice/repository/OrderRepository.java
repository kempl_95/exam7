package kg.exam.foodservice.repository;

import kg.exam.foodservice.model.Dish;
import kg.exam.foodservice.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderRepository extends PagingAndSortingRepository<Order, String> {
    public Page<Order> findAllByUserId(String id, Pageable pageable);
}
