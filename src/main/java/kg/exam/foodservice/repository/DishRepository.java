package kg.exam.foodservice.repository;

import kg.exam.foodservice.model.Dish;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DishRepository extends PagingAndSortingRepository<Dish, String> {
    public Page<Dish> findAllByPlaceId(String id, Pageable pageable);
    List<Dish> getFirstByIdAndPlaceId(String id, String placeId);
}
