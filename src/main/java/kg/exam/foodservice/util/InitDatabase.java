package kg.exam.foodservice.util;

import kg.exam.foodservice.model.Dish;
import kg.exam.foodservice.model.Order;
import kg.exam.foodservice.model.Place;
import kg.exam.foodservice.model.User;
import kg.exam.foodservice.repository.DishRepository;
import kg.exam.foodservice.repository.PlaceRepository;
import kg.exam.foodservice.repository.OrderRepository;
import kg.exam.foodservice.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Configuration
public class InitDatabase {

    private static final Random r = new Random();
    private static final int userQty = 100;
    private static final int placeQty = 100;
    private static final int dishesQty = 100;
    private static final int ordersQty = 70;

    @Bean
    CommandLineRunner init(UserRepository users, DishRepository dishes, OrderRepository orders, PlaceRepository places) {
        return (args) -> {
            dishes.deleteAll();
            orders.deleteAll();
            users.deleteAll();
            places.deleteAll();

            //Добавляю пользователей
            List<User> userList = new ArrayList<>();
            userList.add(User.admin());
            userList.add(User.guest());
            for (int i=0; i<userQty; i++){
                userList.add(User.random());
            }
            users.saveAll(userList);

            //Добавляю Заведения
            List<Place> placeList = new ArrayList<>();
            for (int i=0; i<placeQty; i++){
                placeList.add(Place.random());
            }
            places.saveAll(placeList);

            //Добавляю блюда
            List<Dish> dishList = new ArrayList<>();
            for (int i=0; i<dishesQty; i++){
                dishList.add(Dish.random(placeList.get(r.nextInt(placeList.size()-1))));
            }
            dishes.saveAll(dishList);

            //Добавляю Заказы
            List<Order> orderList = new ArrayList<>();
            for (int i=0; i<ordersQty; i++){
                orderList.add(Order.random(userList.get(r.nextInt(userList.size()-1)),dishList.get(r.nextInt(dishList.size()-1))));
            }
            orders.saveAll(orderList);
            System.out.println("=================== DONE ===================");
            System.out.println();
            System.out.println("Test users: ");
            System.out.println("| Email: admin@gmail.com | password: admin |");
            System.out.println("| Email: guest@gmail.com | password: guest |");
        };
    }
}
