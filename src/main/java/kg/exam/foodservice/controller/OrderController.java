package kg.exam.foodservice.controller;

import kg.exam.foodservice.annotations.ApiPageable;
import kg.exam.foodservice.dto.*;
import kg.exam.foodservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/orders")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @ApiPageable
    @GetMapping
    public Slice<OrderDTO> findOrders(@ApiIgnore Pageable pageable) {
        return orderService.findOrders(pageable);
    }

    //Мы можем из любого заведения заказать любое из блюд
    @PostMapping(path = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponceDTO addSubscription(Authentication authentication, @RequestBody AddOrderDTO data) {
        return orderService.addOrder(authentication, data);
    }
    //Мы можем просмотреть список своих заказов
    @GetMapping(path = "/myOrders")
    public Slice<OrderDTO> findAuthUserOrders(Authentication authentication, @ApiIgnore Pageable pageable) {
        return orderService.findAuthUserOrders(authentication, pageable);
    }


}
