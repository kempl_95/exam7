package kg.exam.foodservice.controller;

import kg.exam.foodservice.annotations.ApiPageable;
import kg.exam.foodservice.dto.DishDTO;
import kg.exam.foodservice.dto.PlaceDTO;
import kg.exam.foodservice.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/places")
public class PlaceController {
    @Autowired
    private PlaceService placeService;

    @ApiPageable
    @GetMapping
    public Slice<PlaceDTO> findPlaces(@ApiIgnore Pageable pageable) {
        return placeService.findPlaces(pageable);
    }

    @GetMapping(path = "/{id}/dishes")
    public Slice<DishDTO> findPublications(@PathVariable String id, @ApiIgnore Pageable pageable) {
        return placeService.findPlaceDishes(id ,pageable);
    }

}
