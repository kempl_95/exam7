package kg.exam.foodservice.controller;

import kg.exam.foodservice.annotations.ApiPageable;
import kg.exam.foodservice.dto.DishDTO;
import kg.exam.foodservice.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/dishes")
public class DishController {
    @Autowired
    private DishService dishService;

    @ApiPageable
    @GetMapping
    public Slice<DishDTO> findDishes(@ApiIgnore Pageable pageable) {
        return dishService.findDishes(pageable);
    }




}
