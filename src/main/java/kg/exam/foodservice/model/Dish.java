package kg.exam.foodservice.model;


import kg.exam.foodservice.util.data.GenerateData;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Random;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection="dishes")
public class Dish {
    public static Dish random(Place place) {
        return builder()
                .name(GenerateData.randomDish().name)
                .type(GenerateData.randomDish().type)
                .price(r.nextInt(100)+1)
                .place(place)
                .build();
    }

    private static final Random r = new Random();

    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    @Indexed
    private String name;
    @Indexed
    private String type;
    @Indexed
    private int price;
    @DBRef
    private Place place;

}
