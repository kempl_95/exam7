package kg.exam.foodservice.model;

import kg.exam.foodservice.util.Generator;
import kg.exam.foodservice.util.data.GenerateData;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection="users")
public class User implements UserDetails {
    public static User random() {
        return builder()
                .email(GenerateData.randomEmail())
                .name(GenerateData.randomPersonName())
                .password(new BCryptPasswordEncoder().encode(Generator.makePassword()))
                .build();
    }
    public static User admin() {
        return builder()
                .email("admin@gmail.com")
                .name("Admin")
                .password(new BCryptPasswordEncoder().encode("admin"))
                .build();
    }
    public static User guest() {
        return builder()
                .email("guest@gmail.com")
                .name("Guest")
                .password(new BCryptPasswordEncoder().encode("guest"))
                .build();
    }
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    @Indexed
    private String name;
    @Indexed
    private String email;
    private String password;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("FULL"));
    }
    @Override
    public String getUsername() {
        return getEmail();
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
}
