package kg.exam.foodservice.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection="orders")
public class Order {
    public static Order random(User user, Dish dish) {
        return builder()
                .user(user)
                .dish(dish)
                .date(LocalDateTime.now().toString())
                .build();
    }

    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    @DBRef
    private User user;
    @DBRef
    private Dish dish;
    private String date;

}
