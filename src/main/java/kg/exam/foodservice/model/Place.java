package kg.exam.foodservice.model;


import kg.exam.foodservice.util.data.GenerateData;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection="places")
public class Place {

    public static Place random() {
        return builder()
                .name(GenerateData.randomPlace().name)
                .description(GenerateData.randomPlace().description)
                .build();
    }

    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    @Indexed
    private String name;
    private String description;

}
